#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_vida.mk

COMMON_LUNCH_CHOICES := \
    lineage_vida-user \
    lineage_vida-userdebug \
    lineage_vida-eng
